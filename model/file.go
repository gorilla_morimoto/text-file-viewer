package model

import (
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"path/filepath"
)

// 対象ファイルを追加する場合は、ここに拡張子を追加する
var (
	targets = []string{
		".txt",
		".md",
	}
)

type File struct {
	// materializeのmodal-triggerのID（#modal+IDとする）にするためのID
	// パス名のmd5チェックサム
	ID       int
	FileName string
	Contents string
	Digest   string // 内容の先頭100文字 || 100文字に満たない場合はすべて
	Path     string
}

func Files(root string) []File {
	log.Println("DEBUG: model.Files() called")
	var list []File

	filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if contains(targets, filepath.Ext(path)) {
			b, err := ioutil.ReadFile(path)
			if err != nil {
				log.Println("ERROR: failed to read", path)
				return nil
			}

			f := File{
				ID:       rand.Int(),
				FileName: filepath.Base(path),
				Contents: string(b),
				Digest:   mkDigest(b),
				Path:     filepath.Dir(path),
			}

			list = append(list, f)
			return nil
		}
		return nil
	})
	return list
}

func mkDigest(b []byte) (digest string) {
	if len(string(b)) > 100 {
		return string(b)[0:100]
	}
	return string(b)
}

func contains(sl []string, s string) bool {
	for _, v := range sl {
		if v == s {
			return true
		}
	}
	return false
}
