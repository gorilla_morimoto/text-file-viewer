package main

import (
	"gitlab.com/gorilla_morimoto/text-file-viewer/routes"
)

func main() {
	routes.RunServer()
}
